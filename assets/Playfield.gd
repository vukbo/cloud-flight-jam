extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ball_has_exited

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Playfield_body_exited(body):
	if body.is_in_group("player"):
		get_tree().queue_delete(body)
		emit_signal("ball_has_exited")
	pass # Replace with function body.
