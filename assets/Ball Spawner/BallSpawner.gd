extends Node2D

export(PackedScene) var ball_reference : PackedScene

export(float) var shooting_speed

var ball_exists = false
var ball_instance

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _spawn_ball():
	if ball_exists: # remove ball if it exists to "reset" it
		get_parent().remove_child(ball_instance);
	#create new ball
	ball_instance = ball_reference.instance()
	ball_instance.position = position
	ball_instance.apply_central_impulse(transform.y * shooting_speed)
	get_parent().add_child(ball_instance)
	ball_exists = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("spawn_ball"):
		_spawn_ball()
#	pass
