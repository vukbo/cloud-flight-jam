extends Node2D

export(PackedScene) var ball_reference : PackedScene

export(float) var shooting_speed

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _spawn_ball():
	if get_tree().is_in_group("player"):
		var ball_instance = ball_reference.instance()
		ball_instance.position = position
		ball_instance.apply_central_impulse(transform.y * shooting_speed)
		get_parent().add_child(ball_instance)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_down"):
		_spawn_ball()
#	pass
