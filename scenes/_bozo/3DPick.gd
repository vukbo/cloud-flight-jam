extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	var camera = get_viewport().get_camera() #Gets Camera currently used
	var from = camera.project_ray_origin(get_viewport().get_mouse_position())
	var to = from + camera.project_ray_normal(get_viewport().get_mouse_position()) * 10000
	var result = get_world().direct_space_state.intersect_ray(from,to, [self])
	var local_coordinates = to_local(result.get("position",Vector3.ZERO) + Vector3(0.5,0.5,0))
	print($Viewport/Node2D.to_local(Vector2(local_coordinates.x,local_coordinates.y)))
#	$Viewport/Node2D/Sprite.position = Vector2(local_coordinates.x,local_coordinates.y)
	
