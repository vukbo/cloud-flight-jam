extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal finished_level

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ExitHole2dArea_body_entered(body):
	if body.is_in_group("player"):
		get_tree().queue_delete(body)
		emit_signal("finished_level")
		yield(get_tree().create_timer(1.0), "timeout")
		SceneManager.load_next_scene()
