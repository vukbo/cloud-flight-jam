extends Node2D

const InkTypes = preload("res://resources/scripts/InkTypes.gd").InkTypes
signal ink_changed(min_ink,max_ink,current_amount) #Fires when Ink changes

export(Dictionary) var init_ink_dict
var ink_dict
var current_ink setget current_ink_set, current_ink_get # The Current Selected Ink

func _ready():
	current_ink = InkTypes.normal # Inits current Ink as normal
	ink_dict = {
		InkTypes.normal : init_ink_dict[0],
		InkTypes.bouncy : init_ink_dict[1]
	}
	
func draw_ink(amount, ink_type):
	ink_dict[ink_type] -= amount
	emit_signal("ink_changed",0,init_ink_dict[ink_type],ink_dict[ink_type])
	
func is_ink_enough(amount, ink_type):
	return ink_dict[ink_type] - amount >= 0
	
func refill_ink(amount, ink_type):
	ink_dict[ink_type] += amount
	emit_signal("ink_changed",0,init_ink_dict[ink_type],ink_dict[ink_type])


#SETTER & GETTER
func current_ink_set(value):
	current_ink = value
	emit_signal("ink_changed",0,init_ink_dict[current_ink],ink_dict[current_ink])

func current_ink_get():
	return current_ink
