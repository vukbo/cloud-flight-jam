extends Area2D



#Destroy Player Ball
func _on_BallDestroyer_body_entered(body):
	if body.is_in_group("player"):
		body.queue_free()
		push_warning("Player was Destroyed by: " + get_path() + "/" + name)
