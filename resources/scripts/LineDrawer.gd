extends Node2D

const InkManager = preload("res://resources/scripts/InkManager.gd")
const Line = preload("res://resources/scripts/Line.gd")
const InkTypes = preload("res://resources/scripts/InkTypes.gd").InkTypes
const BouncyPhyMaterial = preload("res://resources/PhyMaterials/Bouncy.tres")

signal line_type_changed(line_color)

export var min_delta_distance: int = 10
export var line_width = 1
export var line_color = Color(1,1,1)
export var deleted_line_transparency = 0.1

export(Color) var basic_line_color
export(Color) var bouncy_line_color
export(Dictionary) var line_colors

var last_drawing_point = Vector2()
var last_line: Line
var last_deleted_line: Line2D

onready var eraser = $Eraser
onready var ink_manager : InkManager = $InkManager

func _ready():
	var marker = get_tree().root.find_node("marker",true,false)
	
	print(marker)
	
	connect("line_type_changed",marker,"_ink_type_changed")
	$InkManager.connect("ink_changed",marker,"_ink_changed")

func _process(delta):
	
	# Ink Switching
	switch_ink()
	# Debug feature
	if Input.is_action_just_pressed("restart"):
		get_tree().reload_current_scene()
	
	# Line erasing mechanic
	if Input.is_action_pressed("erase"):
		eraser.transform.origin = get_global_mouse_position()
	
	# Line drawing mechanic
	if Input.is_action_just_pressed("draw"):
		last_line = get_new_line()
		last_line.width = line_width
		last_line.joint_mode = Line2D.LINE_JOINT_ROUND
		last_line.begin_cap_mode = Line2D.LINE_CAP_ROUND
		last_line.end_cap_mode = Line2D.LINE_CAP_ROUND
		last_line.antialiased = true
		add_child(last_line)
		
		last_drawing_point = get_global_mouse_position()
		
	if Input.is_action_pressed("draw"):
		var new_point = get_global_mouse_position()
		var distance = new_point.distance_to(last_drawing_point)
		if distance  > min_delta_distance and ink_manager.is_ink_enough(distance,ink_manager.current_ink):
			last_line.add_point(new_point)
			last_drawing_point = new_point
			ink_manager.draw_ink(distance,ink_manager.current_ink)
			last_line.line_length += distance
			
	if Input.is_action_just_released("draw"):
		var colliders = Node2D.new()
		for i in range(0,last_line.points.size(),1):
			if i+1 < last_line.points.size():
				var static_body = StaticBody2D.new()
				var collision_shape = CollisionShape2D.new()
				var segment = SegmentShape2D.new()
				segment.a = last_line.points[i]
				segment.b = last_line.points[i+1]
				collision_shape.shape = segment
				if last_line.ink_type == InkTypes.bouncy:
					static_body.physics_material_override = BouncyPhyMaterial
				static_body.name = 'Line'
				static_body.add_to_group('line')
				static_body.add_child(collision_shape)
				colliders.add_child(static_body)
				if !last_line.is_a_parent_of(colliders):
					last_line.add_child(colliders)

# Needed for refilling mechanic otherwise it would add the amount twice
var refilled_line
# Line Erasing Mechanic & Transparent Line Mechanic
func _on_Eraser_body_entered(body):
	if body.is_in_group('line'):
		if last_deleted_line != null and last_deleted_line != body.get_parent().get_parent():
			last_deleted_line.queue_free()
		last_deleted_line = body.get_parent().get_parent()
		if refilled_line != body.get_parent().get_parent():
			var line = (body.get_parent().get_parent() as Line)
			ink_manager.refill_ink(line.line_length, line.ink_type)
			refilled_line = body.get_parent().get_parent()
		body.get_parent().queue_free()
		last_deleted_line.default_color.a = deleted_line_transparency

func switch_ink():
	if Input.is_action_just_pressed("normal_ink"):
		ink_manager.current_ink = InkTypes.normal
		emit_signal("line_type_changed",line_colors[0])
		
	if Input.is_action_just_pressed("bouncy_ink"):
		ink_manager.current_ink = InkTypes.bouncy
		emit_signal("line_type_changed",line_colors[1])
		
func get_new_line():
	var line 	
	if ink_manager.current_ink == InkTypes.normal:
		line = Line.new(InkTypes.normal)
		line.default_color = line_colors[0]
	elif ink_manager.current_ink == InkTypes.bouncy:
		line = Line.new(InkTypes.bouncy)
		line.default_color = line_colors[1]
	return line
