extends Node

var scenes = []
var scene_dir_path = "res://scenes/release"

var current_scene = 0

onready var viewport = get_tree().root.get_node("Playground3D/Whiteboard/Sprite3D/Viewport")

func _ready():
	load_scenes()
	load_next_scene()

func load_scenes():
	var scene_dir = Directory.new()
	scene_dir.open(scene_dir_path)
	scene_dir.list_dir_begin(true)
	var scene = scene_dir.get_next()
	while scene != '':
		if '.tscn' in scene and not '3D_Overlay.tscn' in scene:
			scenes.append(load(scene_dir_path+"/"+scene))
		scene = scene_dir.get_next()

func load_next_scene():
	if current_scene < scenes.size(): 
		viewport.remove_child(viewport.get_child(0))
		viewport.add_child(scenes[current_scene].instance())
		current_scene += 1
		
