extends Spatial

export(Color) var marker_color

onready var drawing_particles = $DrawingParticles #Fetches the Drawing Particles

# Called when the node enters the scene tree for the first time.
func _ready():
	var line_drawer = get_tree().root.find_node("LineDrawer",true,false)
	
	
#	line_drawer.connect("line_type_changed",self,"_ink_type_changed")
#	line_drawer.get_node("InkManager").connect("ink_changed",self,"_ink_changed")
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var camera = get_viewport().get_camera() #Gets Camera currently used
	var from = camera.project_ray_origin(get_viewport().get_mouse_position())
	var to = from + camera.project_ray_normal(get_viewport().get_mouse_position()) * 1000
	var result = get_world().direct_space_state.intersect_ray(from,to, [self])
	
	translation = result.get("position",Vector3.ZERO)

# Changes the marker Color
func _change_marker_color(color : Color):
	$MarkerTexture.get_active_material(2).albedo_color = color

# Changes Color when Ink Type is changed
func _ink_type_changed(ink_color):
	_change_marker_color(ink_color)

# Alway Called when ink amount changes
func _ink_changed(min_ink,max_ink,current_amount):
	$InkAmountOrigin.ink_changed(min_ink,max_ink,current_amount)

func _unhandled_input(event):
	if event is InputEventMouseButton:
		drawing_particles.emitting = Input.is_action_pressed("draw")
